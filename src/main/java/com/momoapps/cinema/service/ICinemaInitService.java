package com.momoapps.cinema.service;

public interface ICinemaInitService {
    public void intVilles();

    public void initCinemas();

    public void initSalles();

    public void initPlaces();

    public void intSeances();

    public void initCategories();

    public void initFilms();

    public void initProjections();

    public void initTickets();

}
