package com.momoapps.cinema.service;

import com.momoapps.cinema.dao.*;
import com.momoapps.cinema.entities.*;
import org.hibernate.criterion.Projection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

@Service
@Transactional
public class CinemaInitServiceImpl implements ICinemaInitService {
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private CinemaRepository cinemaRepository;
	@Autowired
	private SalleRepository salleRepository;
	@Autowired
	private PlaceRepository placeRepository;
	@Autowired
	private FilmRepository filmRepository;
	@Autowired
	private SeanceRepository seanceRepository;
	@Autowired
	private TicketRepository ticketRepository;
	@Autowired
	private ProjectionRepository projectionRepository;
	@Autowired
	private CategorieRepository categorieRepository;

	@Override
	public void intVilles() {
		Stream.of("Casablanca", "Bamako", "Gao", "Niamey", "Barcelona", "Los Angeles").forEach(nameCity -> {
			Ville ville = new Ville();
			ville.setName(nameCity);
			villeRepository.save(ville);
		});
	}

	@Override
	public void initCinemas() {
		villeRepository.findAll().forEach(city -> {
			Stream.of("MegaRama", "BaBemBa", "Sony", "Djongorjo", "Hasta La Vista", "Hollywood Park")
					.forEach(cineName -> {
						Cinema cinema = new Cinema();
						cinema.setNombreDeSalles(3 + (int) (Math.random() * 7));
						cinema.setName(cineName);
						cinema.setVille(city);
						cinemaRepository.save(cinema);
					});
		});
	}

	@Override
	public void initSalles() {
		cinemaRepository.findAll().forEach(cinema -> {
			for (int i = 0; i < cinema.getNombreDeSalles(); i++) {
				Salle salle = new Salle();
				salle.setName("Salle " + (i + 1));
				salle.setCinema(cinema);
				salle.setNombrePlaces(15 + (int) (Math.random() * 20));
				salleRepository.save(salle);
			}
		});
	}

	@Override
	public void initPlaces() {
		salleRepository.findAll().forEach(salle -> {
			for (int i = 0; i < salle.getNombrePlaces(); i++) {
				Place place = new Place();
				place.setNumero(i + 1);
				place.setSalle(salle);
				placeRepository.save(place);
			}
		});
	}

	@Override
	public void intSeances() {
		Stream.of("12:00", "13:00", "15:00", "17:00", "19:00", "21:00").forEach(s -> {
			DateFormat dateFormat = new SimpleDateFormat("HH:mm");
			Seance seance = new Seance();
			try {
				seance.setHeureDebut(dateFormat.parse(s));
			} catch (ParseException e) {
			}
			seanceRepository.save(seance);
		});
	}

	@Override
	public void initCategories() {
		Stream.of("Action", "Histoire", "Drama", "Fiction", "Trailler", "Suspens").forEach(catName -> {
			Categorie categorie = new Categorie();
			categorie.setName(catName);
			categorieRepository.save(categorie);
		});
	}

	@Override
	public void initFilms() {
		double[] durees = new double[] { 1, 1.5, 2, 2.5, 3 };
		List<Categorie> categories = categorieRepository.findAll();
		Stream.of("Game Of Throne", "Scareface", "IronMan", "Double vie", "Cat Women").forEach(title -> {
			Film film = new Film();
			film.setTitre(title);
			film.setDuree(durees[new Random().nextInt(durees.length)]);
			film.setPhoto(title.replaceAll(" ", "") + ".jpg");
			film.setCategorie(categories.get(new Random().nextInt(categories.size())));
			filmRepository.save(film);
		});
	}

	@Override
	public void initProjections() {
		double[] prix = new double[] { 30, 50, 60, 70, 80, 90, 100 };
		List<Film> films = filmRepository.findAll();
		villeRepository.findAll().forEach(city -> {
			city.getCinemas().forEach(cinema -> {
				cinema.getSalles().forEach(salle -> {
					// Choisir un film de facon aleatoire dans chaque salle
					int index = new Random().nextInt(films.size());
					Film film = films.get(index);
					seanceRepository.findAll().forEach(seance -> {
						ProjectionFilm projectionFilm = new ProjectionFilm();
						projectionFilm.setDateProjection(new Date());
						projectionFilm.setFilm(film);
						projectionFilm.setPrix(prix[new Random().nextInt(prix.length)]);
						projectionFilm.setSalle(salle);
						projectionFilm.setSeance(seance);
						projectionRepository.save(projectionFilm);
					});
				});
			});
		});
	}

	@Override
	public void initTickets() {
		projectionRepository.findAll().forEach(projection -> {
			projection.getSalle().getPlaces().forEach(place -> {
				Ticket ticket = new Ticket();
				ticket.setPlace(place);
				ticket.setPrix(projection.getPrix());
				ticket.setProjectionFilm(projection);
				ticket.setReserve(false);
				ticketRepository.save(ticket);
			});
		});
	}
}
