package com.momoapps.cinema.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class Ticket implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nomClient;
    private double prix;
    @Column(unique = false, nullable = true)
    private Integer codePaiement;
    private boolean reserve;
    @ManyToOne
    @JoinColumn(name = "place_fk")
    private Place place;
    @ManyToOne
    @JoinColumn(name = "projection_fk")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private ProjectionFilm projectionFilm;
}
