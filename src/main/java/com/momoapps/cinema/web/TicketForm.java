package com.momoapps.cinema.web;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
class TicketForm {
    private String nomClient;
    private int codePaiement;
    private List<Long> tickets = new ArrayList<>();
}