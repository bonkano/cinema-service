package com.momoapps.cinema.dao;

import com.momoapps.cinema.entities.ProjectionFilm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(/* path = "projections" */)
@CrossOrigin("*")
public interface ProjectionRepository extends JpaRepository<ProjectionFilm, Long> {
}
