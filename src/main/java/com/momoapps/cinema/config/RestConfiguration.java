package com.momoapps.cinema.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import com.momoapps.cinema.entities.Categorie;
import com.momoapps.cinema.entities.Cinema;
import com.momoapps.cinema.entities.Film;
import com.momoapps.cinema.entities.Place;
import com.momoapps.cinema.entities.ProjectionFilm;
import com.momoapps.cinema.entities.Salle;
import com.momoapps.cinema.entities.Seance;
import com.momoapps.cinema.entities.Ticket;
import com.momoapps.cinema.entities.Ville;

@Configuration
public class RestConfiguration implements RepositoryRestConfigurer {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		/*
		 * List<Object> entities = Arrays.asList(Categorie.class, Cinema.class,
		 * Film.class, Place.class, ProjectionFilm.class, Salle.class, Seance.class,
		 * Ticket.class, Ville.class); entities.forEach(entity -> {
		 * config.exposeIdsFor(entity); });
		 */
		config.exposeIdsFor(Salle.class, Seance.class, Categorie.class, Cinema.class, Place.class, ProjectionFilm.class,
				Ticket.class, Ville.class);

		// config.setBasePath("/api")
	}

}
